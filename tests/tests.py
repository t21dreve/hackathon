from pydoc import pager
import pytest

import db
from app import app as my_app, fetch_data


# Configuring the app for testing
@pytest.fixture()
def app():
    app = my_app
    db.create_database("./data/database.json")
    app.config.update({
        "TESTING": True,
    })

    yield app


# Configuring the client and runner for testing (these are flask methods, not custom ones)
@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


# Testing if home page is working and responding correctly
def test_home_page(client):
    response = client.get("/")
    assert response.status_code == 200


# Testing if the event list is working
def test_longtrip_list(client):
    # Testing if we can access the default list of events
    response = client.get("/trip/long")
    assert response.status_code == 200


def test_shorttrip_list(client):
    # Testing if we can access the default list of events
    response = client.get("/trip/short")
    assert response.status_code == 200


# Testing if the current content of the three pages are right
def test_pages_content_home(client):
    # Testing if the home page is displaying the right content
    response = client.get("/")
    page_data = str(response.data).split("\\n")
    page_data = [s.strip() for s in page_data]

    # We are looking for the page title, and the three links
    elements_to_find = [
        '<h1> Accueil </h1>',
        '<h2>Nos services</h2>',
        '<li><a class="btn btn-outline-dark mt-2" href="/trip/short"> Liste Les Trajets Courts </a></li>',
        '<li><a class="btn btn-outline-dark mt-2" href="/trip/long"> Liste Les Trajets Longs </a></li>',
        '<button class="btn btn-outline-dark mt-2" type="submit"> Cr\\xc3\\xa9er un Trajet </button>',
    ]

    print(page_data)

    for element in elements_to_find:
        assert element in page_data


def test_pages_content_list(client):
    # Testing if the home page is displaying the right content
    response = client.get("/trip/long")
    page_data = str(response.data).split("\\n")
    page_data = [s.strip() for s in page_data]

    # We are looking for the page long trip list, and the table it displays
    elements_to_find = [
        '<h1>Trajets longs</h1>',
        '<a href="/" class="btn btn-secondary"> < Back </a>',
        '<table class="table">',
        '<tr style="text-align:center">',
        '<th>D\\xc3\\xa9part</th>',
        '<th>Arriv\\xc3\\xa9e</th>',
        '<th>Date</th>',
        '<th>Description</th>',
        '<th>Conducteur</th>',
        '<th>Places restantes</th>',
        '<th>D\\xc3\\xa9tails</th>'
    ]

    print(page_data)

    for element in elements_to_find:
        assert element in page_data


def test_pages_content_detail(client):
    # Testing if the home page is displaying the right content
    response = client.get("/trip/details/1")
    page_data = str(response.data).split("\\n")
    page_data = [s.strip() for s in page_data]

    # We are looking for the page title, and the three search inputs
    elements_to_find = [
        '<h1> Trajet </h1>',
        '<a href="/trip/long" class="btn btn-secondary"> < Back </a>',
        '<li>Adresse d\\xc3\\xa9part : All\\xc3\\xa9e Jean Baptise Fourier</li>',
        '<li>Adresse arriv\\xc3\\xa9e : Rue de l&#39;\\xc3\\xa9glise</li>',
        '<li>Date : 01/02/2024 12:00</li>',
        '<li>Conducteur : John Doe</li>',
        '<li>Contact : 06 06 06 06 06</li>',
        '<li>Description: Je pars demain de Nantes pour rentrer \\xc3\\xa0 Brest</li>',
        '<li>Nombre de places disponibles : 4</li>',
        '<button class="btn btn-outline-danger" onclick="fetch(\\\'/trip/1\\\', { method: \\\'DELETE\\\' }); location.href = \\\'/trip/long\\\'">Supprimer le trajet</button>',
    ]

    print(page_data)

    for element in elements_to_find:
        assert element in page_data

##############################################################
# Testing the trip creation & accessing the trip detailed page
##############################################################

# We will follow the creation of a trip from the home page to the very end    
expected_payload = {
    "id": 0,
    "first_name": "FirstName",
    "last_name": "LastName",
    "description": "Description",
    "contact": "Contact",
    "adresse_depart": "AdresseDepart",
    "adresse_arrivee": "AdresseArrivee",
    "date": "1970-01-01",
    "heure": "18:45",
    "places_disponibles": 5,
    "passengers": []
}

# We begin by calling the initial trip creation page
def test_trip_creation_deletion_informations(client):
    response = client.post("/trip/create")
    assert response.status_code == 302

    # The created trip should be empty, except for the id that should have been automatically set to 0 since this is the first id that was not in use.
    trip = db.get_trip(0)
    # We just check that the trip do exists
    assert trip

    # After that, users are sent to the page to choose the starting adress of the trip
    response = client.get("/trip/create/0")
    assert response.status_code == 200

    # We will now try to set the starting adress of the trip
    response = client.post("/trip/create2/0", data={
        "place": "AdresseDepart"
    })

    # Checking if the trip has been correctly updated
    trip = db.get_trip(0)
    assert trip["adresse_depart"] == "AdresseDepart"

    # Now, we check the next form to choose the end adress of the trip
    response = client.get("/trip/create2/0")
    assert response.status_code == 200

    # We will now try to set the end adress of the trip
    response = client.post("/trip/create3/0", data={
        "place": "AdresseArrivee"
    })
    assert response.status_code == 302

    # Checking if the trip has been correctly updated
    trip = db.get_trip(0)
    assert trip["adresse_arrivee"] == "AdresseArrivee"

    # Now, we check the next form to input all other informations about the trip
    response = client.get("/trip/create3/0")
    assert response.status_code == 200

    # We will now try to set the other data of the trip
    response = client.post("/trip/create4/0", data={
        "first_name": "FirstName",
        "last_name": "LastName",
        "description": "Description",
        "contact": "Contact",
        "date": "1970-01-01",
        "heure": "18:45",
        "places_disponibles": 5
    })
    assert response.status_code == 302

    # Checking if the trip has been correctly updated
    trip = db.get_trip(0)
    for key in ["first_name", "last_name", "description", "contact", "date", "heure", "places_disponibles"]:
        assert trip[key]

    # Finally, we check if the trip is correctly displayed on the detailed page
    response = client.get("/trip/details/0")
    assert response.status_code == 200

    # Before ending the test, we delete the trip we created
    db.delete_trip(0)

    # Checking if the trip has been correctly deleted
    trip = db.get_trip(0)
    assert not trip

def test_add_passenger(client):
    response = client.get("/trip/details/1")
    assert response.status_code == 200

    trip = db.get_trip(1)
    assert len(trip["passengers"]) == 2

    response = client.post("/trip/1/addpassenger", data={
        "first_name": "FirstName",
        "last_name": "LastName",
        "contact": "Contact",
    })

    assert response.status_code == 302

    trip = db.get_trip(1)
    assert len(trip["passengers"]) == 3

    trip = db.get_trip(2)
    assert len(trip["passengers"]) == 1


    response = client.post("/trip/2/addpassenger", data={
        "first_name": "FirstName",
        "last_name": "LastName",
        "contact": "Contact",
    })
    assert response.status_code == 302

    trip = db.get_trip(2)
    assert len(trip["passengers"]) == 1

def test_delete_passenger(client):
    """
    Test deleting a passenger
    :param client:
    :return:
    """
    response = client.get("/trip/details/1")
    assert response.status_code == 200

    passenger_id = db.get_passenger_id(1)

    response = client.post("/trip/1/addpassenger", data={
        "first_name": "FirstName",
        "last_name": "LastName",
        "contact": "Contact",
    })
    assert response.status_code == 302

    response = client.delete(f"/trip/1/passenger/{passenger_id}")
    assert response.status_code == 204

    trip = db.get_trip(1)
    for p in trip["passengers"]:
        assert int(p["id"]) != int(passenger_id)

    response = client.get("/trip/details/2")
    assert response.status_code == 200

    passenger_id = db.get_passenger_id(2)

    response = client.post("/trip/2/addpassenger", data={
        "first_name": "FirstName",
        "last_name": "LastName",
        "contact": "Contact",
    })

    assert response.status_code == 302

    response = client.delete(f"/trip/2/passenger/{passenger_id}")
    assert response.status_code == 204

    trip = db.get_trip(2)
    for p in trip["passengers"]:
        assert int(p["id"]) != int(passenger_id)

def test_add_comment(client):
    # We will add a comment to the first passenger of the first trip and check that it has been correctly added
    comment = {
        "first_name": "John",
        "last_name": "Doe",
        "content": "Je te récupère à 18h30"
    }

    response = client.post("/trip/1/passenger/0/comment", data=comment)
    assert response.status_code == 302

    # Now we get the comment page to check if everything went well
    response = client.get("/trip/1/passenger/0/comment")
    assert response.status_code == 200

    # Now we check if the comment was added in the database
    trip = db.get_trip(1)
    passenger = trip["passengers"][0]
    assert passenger["comments"][0]["first_name"] == comment["first_name"]
    assert passenger["comments"][0]["last_name"] == comment["last_name"]
    assert passenger["comments"][0]["content"] == comment["content"]