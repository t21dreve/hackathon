import datetime

from flask import Flask, render_template, request, flash, redirect, url_for, make_response, jsonify

import requests
import json
import os

import db

app = Flask(__name__)

PORT = int(os.environ.get("PORT", 5000))  # For Scalingo deployment
HOST = "0.0.0.0"


# Endpoint for the home page, it features a search bar with suggestions for places and authors
@app.route('/', methods=["GET"])
def home():

    return render_template('app/trips/home.html')

# Endpoint for the list of events, it features a pagination system
@app.route('/trip/short')
def short_trips():
    """
    Display the list of short trips
    This page is not implemented for the moment
    :return:
    """

    return render_template('app/trips/short_trips.html')

@app.route('/trip/long')
def long_trips():
    """
    Display the list of long trips
    :return:
    """
    trips = db.get_data()
    return render_template('app/trips/long_trips.html', trips=trips)

@app.route('/trip/create', methods=["POST"])
def create_trip():
    """
    create a new trip
    :return:
    """
    id = db.create_trip()
    return redirect(url_for("create_trip_step1", id=id), code=302)

@app.route('/trip/create/<id>', methods=["GET"])
def create_trip_step1(id):
    # We look for url parameters
    departement = request.args.get('departement', None)
    if departement:
        departement = int(departement)
    search = request.args.get('search', None)
    if search:
        search = str(search)

    parkings = fetch_data(departement, search)
    return render_template("app/trips/create1.html", id=id, parkings = parkings)

@app.route('/trip/create2/<id>', methods=["POST"])
def create_trip_step2(id):
    print(request.form)
    db.update_trip(int(id), {
        "adresse_depart": request.form.get("other") if request.form.get("other") else request.form.get("place")
    })
    return redirect(url_for("create_trip_step3", id=id), code=302)

@app.route('/trip/create2/<id>', methods=["GET"])
def create_trip_step3(id):
    # We look for url parameters
    departement = request.args.get('departement', None)
    if departement:
        departement = int(departement)
    search = request.args.get('search', None)
    if search:
        search = str(search)

    parkings = fetch_data(departement, search)
    return render_template("app/trips/create2.html", id=id, parkings=parkings)

@app.route('/trip/create3/<id>', methods=["POST"])
def create_trip_step4(id):
    db.update_trip(int(id), {
        "adresse_arrivee": request.form.get("other") if request.form.get("other") else request.form.get("place")
    })
    return redirect(url_for("create_trip_step5", id=id), code=302)

@app.route('/trip/create3/<id>', methods=["GET"])
def create_trip_step5(id):
    return render_template("app/trips/create3.html", id=id)

@app.route('/trip/create4/<id>', methods=["POST"])
def create_trip_step6(id):
    db.update_trip(int(id), {
        "first_name": request.form.get("first_name"),
        "last_name": request.form.get("last_name"),
        "contact": request.form.get("contact"),
        "description": request.form.get("description"),
        "places_disponibles": int(request.form.get("places_disponibles")),
        "date": datetime.date.fromisoformat(request.form.get("date")).strftime("%d/%m/%Y") ,
        "heure": request.form.get("heure")
    })
    return redirect(url_for("trip_details", id=id), code=302)

@app.route('/trip/details/<id>', methods=["GET"])
def trip_details(id):
    """
    Get the trip details
    :param id: id of trip
    :return:
    """
    trip = db.get_trip(int(id))
    return render_template("app/trips/trip_details.html", trip=trip)

@app.route('/trip/<id>/addpassenger', methods=["POST"])
def add_passenger(id):
    """
    add a new passenger for the trip with trip id
    :return:
    """
    # get the necessary information of the passenger
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    contact = request.form['contact']
    # if no the necessary information, send alert
    if not first_name:
        flash('Un prénom est requis')
    if not last_name:
        flash('Un nom est requis')
    # add the passenger to the database
    db.add_passenger(id, first_name, last_name, contact)
    print('passenger added!')
    # reload the trip details page
    return redirect(url_for('trip_details', id=id), code=302)

@app.route("/trip/<id>", methods=["DELETE"])
def delete_trip(id):
    """
    delete a trip according to the trip id
    :param id: trip id
    :return:
    """
    id = int(id)
    if isinstance(id, int):
        db.delete_trip(id)
    return make_response(None, 204)

@app.route("/trip/<trip_id>/passenger/<passenger_id>", methods=["DELETE"])
def delete_passenger(trip_id, passenger_id):
    """
    delete a passenger
    :param trip_id:
    :param passenger_id:
    :return:
    """
    trip_id = int(trip_id)
    passenger_id = int(passenger_id)
    if isinstance(trip_id, int) and isinstance(passenger_id, int):
        db.delete_passenger(trip_id, passenger_id)
    return make_response(jsonify({"test":"test"}), 204)

@app.route("/trip/<trip_id>/passenger/<passenger_id>/comment", methods=["GET"])
def see_comments(trip_id, passenger_id):
    trip_id = int(trip_id)
    passenger_id = int(passenger_id)
    trip = db.get_trip(trip_id)
    passe = None
    if trip["passengers"]:
        for passenger in trip["passengers"]:
            if passenger["id"] == passenger_id:
                passe = passenger
                break
    else:
        passe = None
    return render_template("app/trips/comments.html", trip_id=trip_id, passenger=passe)
@app.route("/trip/<trip_id>/passenger/<passenger_id>/comment", methods=["POST"])
def comment(trip_id, passenger_id):
    trip_id = int(trip_id)
    passenger_id = int(passenger_id)
    if isinstance(trip_id, int) and isinstance(passenger_id, int):
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        content = request.form['content']
        db.add_comment(trip_id, passenger_id, first_name, last_name, content)

    return redirect(url_for('see_comments', trip_id=trip_id, passenger_id=passenger_id), code=302)

def fetch_data(department = None, keyword = None):
    """
    This function fetches the parking data from the API. You can specify the department to get the parking data for this department only.
    Specifying a department will disable the keyword search included within the function

    Args:
        keyword (str): Keyword for a search
        department (int): Department number

    Returns: (data: List, end: bool)
    """
    if (department):
        url = f"https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/aires-covoiturage/records?where=code_postal%20LIKE%20%22{department}*%22"
    elif (keyword == None):
        url = f"https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/aires-covoiturage/records?where=code_postal%20LIKE%20%2244300%22%20OR%20code_postal%20LIKE%20%2244470%22%20OR%20code_postal%20%3D%20%2244000%22%20OR%20code_postal%20%3D%20%2244036%22%20OR%20code_postal%20%3D%20%2244321%22%20OR%20code_postal%20%3D%20%2244100%22%20OR%20code_postal%20%3D%20%2244325%22%20OR%20code_postal%20%3D%20%2244200%22"
    else:
        url = f"https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/aires-covoiturage/records?where=(code_postal%20%3D%20%2244300%22%20OR%20code_postal%20LIKE%20%2244470%22%20OR%20code_postal%20%3D%20%2244000%22%20OR%20code_postal%20%3D%20%2244036%22%20OR%20code_postal%20%3D%20%2244321%22%20OR%20code_postal%20%3D%20%2244100%22%20OR%20code_postal%20%3D%20%2244325%22%20OR%20code_postal%20%3D%20%2244200%22)%20AND%20(nom_du_lieu%20LIKE%20%22*{keyword}*%22%20OR%20adresse%20LIKE%20%22*{keyword}*%22)"

    url += "&limit=100 "
    # Fetching data
    data = requests.get(url)

    return data.json()["results"]


if __name__ == "__main__":
    db.create_database()
    print("Server running in port %s" % (PORT))
    app.run(host=HOST, port=PORT)