from flask import Flask, render_template, request

import requests
import json
import os

app = Flask(__name__)

PORT = int(os.environ.get("PORT", 5000)) # For Scalingo deployment
HOST = "0.0.0.0"

# Endpoint for the home page, it features a search bar with suggestions for places and authors
@app.route('/')
def search_events():
    lieu_data, emetteur_data = fetch_search_data()
    return render_template('app/events/home.html', lieux=lieu_data, emetteurs=emetteur_data)


# Endpoint for the list of events, it features a pagination system
@app.route('/events')
def liste_events():
    data, end = fetch_data(index=int(request.args.get('index', 1)), lieu=request.args.get('lieu'), emetteur=request.args.get('emetteur'), limit=int(request.args.get('limit', 20)))
    return render_template('app/events/list_events.html', events=data, index=int(request.args.get('index', 1)), lieu=request.args.get('lieu'), emetteur=request.args.get('emetteur'), limit=int(request.args.get('limit', 20)), end=end)

# Endpoint for individual events
@app.route('/events/<id>')
def event_id(id):
    data, end = fetch_data(id=id)
    return render_template('app/events/event.html', event=data[0])

def fetch_search_data():
    """
    This function fetches the differents available places and authors for the various event.
    It is used to populate the search suggestions in the search bar of the home page

    Returns: (places: List, authors: List)
    """
    lieu_url = "https://data.nantesmetropole.fr/api/explore/v2.1/catalog/datasets/244400404_agenda-evenements-nantes-nantes-metropole/records?select=lieu&where=lieu%20IS%20NOT%20NULL&group_by=lieu&limit=100"
    emetteur_url = "https://data.nantesmetropole.fr/api/explore/v2.1/catalog/datasets/244400404_agenda-evenements-nantes-nantes-metropole/records?select=emetteur&where=emetteur%20IS%20NOT%20NULL&group_by=emetteur&limit=100"

    lieu_data = requests.get(lieu_url)
    emetteur_data = requests.get(emetteur_url)

    return lieu_data.json()["results"], emetteur_data.json()["results"]

def fetch_data(index = 1, limit = 5, lieu = None, emetteur = None, id = None):
    """
    This function fetches the data from the API. It will return the data and a boolean indicating if this is the end of the data or not (for the index system).
    It is used to get list of events and individual events depending of the endpoint used.

    Args:
        index (int): The page index.
        limit (int): The number of events per page.
        lieu (str): The place of the event.
        emetteur (str): The author of the event.
        id (str): The id of the event.
    
    Returns: (data: List, end: bool)
    """
    # Generating API call with index & offset
    if index <= 0:
        index = 1
    offset = index * limit - limit

    url = f"https://data.nantesmetropole.fr/api/explore/v2.1/catalog/datasets/244400404_agenda-evenements-nantes-nantes-metropole/records?order_by=date%20DESC&limit={limit}&offset={offset}"

    # Adding to API call the where query
    if id or lieu or emetteur:
        counter = 0
        url += f"&where="
        if id:
            counter += 1
            url += f"id_manif%20%3D%20%22{id}%22"
        if lieu:
            if counter > 0:
                url += "%20AND%20"
            counter += 1
            url += f"lieu%20%3D%20%22{lieu}%22"
        if emetteur:
            if counter > 0:
                url += "%20AND%20"
            url += f"emetteur%20%3D%20%22{emetteur}%22"
    
    # Fetching data
    data = requests.get(url)

    # Checking if we are at the end of the data or not
    total_count = data.json()["total_count"]
    if (index * limit) > total_count:
        end = True
    else:
        end = False

    
    return data.json()["results"], end


if __name__ == "__main__":
    print("Server running in port %s" % (PORT))
    app.run(host=HOST, port=PORT)