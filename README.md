# Hackathon

Authors : Théo DREVES, Zuoyu ZHANG and Corentin CAUGANT.

## How to launch the application

First you need to download the dependencies :

```bash
$ pip install -r requirements.txt
```

Then you can launch the application with the following command :

```bash
$ python app.py
```

## Description of the application

This application is a little web app that permits to the students of IMT Atlantique to share carpool journeys to the other students.

The application accesses <a href="https://public.opendatasoft.com/explore/dataset/aires-covoiturage/api/?flg=fr-fr&fbclid=IwAR3FG1m8bdAZv5zDgi7JMS8g3-aB4g39dxZJb0F1M19WNDx9TMq6JdSo4RQ">an Open Data about carpool areas in France</a>.

The app feature a home page, displaying links to the other pages of the app. 

You can display the list of short journeys that the students do daily or weekly around Nantes. (This page is not implemented yet)

You can also display the list of long journeys that the student can do between the school and their hometown for example.

When you click on a journey on the list, you can access to its details, and you can book a seat on the car for this journey.

Finally, a page permits you to create and share a new trip on the app. It asks you the address of departure and arrival of your journey. 
To do that, our app can display the list of the carpool areas around Nantes or another department in France that we got through the open data.

Then it asks you additional information to finish the creation of the journey.

The homepage is accessible through the `/` route

The list of the short trips is accessible through the `/trip/short` route (Page not implemented)

The list of the long trips is accessible through the `/trip/long` route

The details of a journey is accessible through the `/trip/details/<id>` route

The page to add a new journey is accessible through the `/trip/create` route

## Technical aspect

The app is coded using flask, all the code is located in `app.py` in the main folder. Tests for the application are located in the `tests` folder, within `tests.py` and uses pytest.

The app use Flask's HTML template system for the display, you can find the HTML files in the `templates` folder, with `base.html` being the layout of each page, and the files in `app` being the individual views for each kind of page.

Do note that bootstrap is used for the styling of the website.

## Contributing

The main branch is protected. All merges to main must be done through the develop branch. Merging won't be allowed if the test pipeline was not passed.

All changes must be committed to the develop branch and then merged to the main branch.

Tests are automatically performed on a remote push through a pipeline made using gitlab CI (see .gitlab-ci.yml in the main folder).

The merge request from develop to main is created automatically on push using gitlab CI.

If tests are successfull (the pipeline is passing) on both the develop and the main branch, the app will be deployed automatically on the <a href="https://dashboard.scalingo.com/apps/osc-fr1/hackathon"> Scalingo host provider </a>

You can then access the app at <a href="https://hackathon.osc-fr1.scalingo.io/"> the following adress </a>
