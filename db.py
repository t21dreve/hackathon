import os
import json

def create_database(filename = "./data/database.json"):
    """
    Check if the database file exists.
    If it does not, create it.
    """
    if not os.path.isfile(filename):
        f = open(filename, "w")
        json.dump(example_dataset, f)
        f.close()

def get_data(filename = "./data/database.json"):
    """
    Returns the dataset in a dictionary
    :return:
    """
    f = open(filename, "r")
    data = json.load(f)
    f.close()
    return data

def save_data(dictionary, filename = "./data/database.json"):
    """
    Save a dictionary in database.json
    :param dictionary:
    :return:
    """
    f = open(filename, "w")
    json.dump(dictionary, f)
    f.close()

def create_trip():
    """
    Create a new trip, and returns its corresponding id
    """
    trips = get_data()

    # We look for an id that does not exist
    id = 0
    while any(trip["id"] == id for trip in trips):
        id += 1

    # We can now initialize our trip and insert it
    new_dataset = {
        "id": id,
        "first_name": None,
        "last_name": None,
        "description": None,
        "contact": None,
        "adresse_depart": None,
        "adresse_arrivee": None,
        "date": None,
        "heure": None,
        "places_disponibles": None,
        "passengers": []
    }

    trips.append(new_dataset)
    save_data(trips)
    return id

def _update_dataset(id, field, value_update, dataset):
    for trip in dataset:
        if trip["id"] == id:
            trip[field] = value_update
            break
    return dataset

def update_trip(id, dataset):
    trips = get_data()

    for key,value in dataset.items():
        trips = _update_dataset(id, key, value, trips)

    save_data(trips)
    return

def delete_trip(id):
    trips = get_data()

    for trip in trips:
        if trip["id"] == id:
            trips.remove(trip)
            break
    
    save_data(trips)

def get_trip(id):
    trips = get_data()
    for trip in trips:
        if trip["id"] == id:
            return trip
    return None

def get_passenger_id(id):
    """
    This function will have a look to the passenger of the trip with the corresponding id, and return the lowest free id
    that could be given to a new passenger
    :param id:
    :return:
    """
    trip = get_trip(id)
    if trip is None or len(trip["passengers"]) == 0:
        return 0
    else:
        id = 0
        while any(passenger["id"] == id for passenger in trip["passengers"]):
            id += 1
        return id

def add_passenger(id, first_name, last_name, contact):

    trips = get_data()

    # Searching for the trip
    trip = None
    for d in trips:
        if d["id"] == int(id):
            trip = d
            break

    # Checking the trip still has places available
    if trip["places_disponibles"] - len(trip["passengers"]) > 0:
        trip["passengers"].append({
            "id": get_passenger_id(id),
            "first_name": first_name,
            "last_name": last_name,
            "contact": contact,
            "comments": []
        })

    save_data(trips)
    return

def delete_passenger(trip_id, passenger_id):
    trips = get_data()

    trip = None
    for d in trips:
        if d["id"] == trip_id:
            trip = d
            break

    for passenger in trip["passengers"]:
        if passenger["id"] == passenger_id:
            trip['passengers'].remove(passenger)
            break

    save_data(trips)

def add_comment(trip_id, passenger_id, first_name, last_name, content):
    trips = get_data()

    trip = None
    for d in trips:
        if d["id"] == trip_id:
            trip = d
            break

    for passenger in trip["passengers"]:
        if passenger["id"] == passenger_id:
            passenger["comments"].append({
                "first_name": first_name,
                "last_name": last_name,
                "content": content
            })
            break

    save_data(trips)

example_dataset = [
    {
        "id": 1,
        "first_name": "John",
        "last_name": "Doe",
        "description": "Je pars demain de Nantes pour rentrer à Brest",
        "contact": "06 06 06 06 06",
        "adresse_depart": "Allée Jean Baptise Fourier",
        "coord_depart": [47.279031, -1.5181603],
        "adresse_arrivee": "Rue de l'église",
        "coord_arrivee": [47.30062991701823, -1.499320463879401],
        "date": "01/02/2024",
        "heure": "12:00",
        "places_disponibles": 4,
        "passengers": [
            {
            "id": 0,
            "first_name": "Jane",
            "last_name": "Doe",
            "contact": "07 07 07 07 07",
            "comments": []
            },
            {
            "id": 1,
            "first_name": "Jane",
            "last_name": "Doe",
            "contact": "07 07 07 07 07",
            "comments": []
            }
        ]
    },
    {
        "id": 2,
        "first_name": "Jane",
        "last_name": "Doe",
        "description": "De Brest à Nantes",
        "contact": "07 07 07 07 07",
        "adresse_depart": "Rue de l'église",
        "adresse_arrivee": "Allée Jean Baptiste Fourier",
        "date": "02/02/2024",
        "heure": "12:00",
        "places_disponibles": 1,
        "passengers": [
            {
            "id": 0,
            "first_name": "John",
            "last_name": "Doe",
            "contact": "06 06 06 06 06",
            "comments": []
            }
        ]
    },
    {
        "id": 3,
        "first_name": "Forever",
        "last_name": "Alone",
        "description": "Je cherche des amis pour rouler",
        "contact": "05 04 03 02 01",
        "adresse_depart": "Allée Jean Baptiste Fourier",
        "adresse_arrivee": "Allée Jean Baptiste Fourier",
        "date": "14/02/2024",
        "heure": "12:00",
        "places_disponibles": 5,
        "passengers": []
    }
]